

# http://stackoverflow.com/a/2262424
from datetime import datetime


def ignore_exception(exception=Exception, default_val=None):
    """ Decorator for ignoring exception from a function
    e.g.   @ignore_exception(DivideByZero)
    e.g.2. ignore_exception(DivideByZero)(Divide)(2/0)
    """
    def dec(function):
        def _dec(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except exception:
                return default_val
        return _dec
    return dec

try_int = ignore_exception(ValueError, None)(int)
try_date_iso = ignore_exception(ValueError, None)(lambda x: datetime.strptime(x, '%Y-%m-%d') if x else None)


def try_date(date_str, default_value, *formats):
    for format in formats:
        try:
            return datetime.strptime(date_str, format)
        except ValueError:
            pass

    return default_value

