from datetime import datetime
from urllib import parse
import werkzeug.datastructures
from flask import Response
from flask import request
from flask_restplus import Api, fields, inputs
from flask_restplus import Resource
from flask_restplus.reqparse import RequestParser, Argument
from werkzeug.exceptions import BadRequest, InternalServerError, NotFound

import repository

FILE_PARAMETER_NAME = 'sgf'

api = Api(prefix="/api/v1", version="1.0")
ns = api.namespace("games")

upload_parser = api.parser()
upload_parser.add_argument(FILE_PARAMETER_NAME, location='files',
                           type=werkzeug.datastructures.FileStorage, required=True, help='file in sgf format')

search_parser = RequestParser()
search_parser\
    .add_argument("player", type=str, required=False, location='args', help="name of one of the players")\
    .add_argument("event",  type=str, required=False, default=None, location='args')\
    .add_argument("orderby", type=str, required=False, default=None, location='args', dest="order_by",
                  help='name of field for sort. Only one field can be specified')\
    .add_argument('orderbydesc', type=str, required=False, default=None, location='args', dest="order_by_desc",
                  help='name of field for sort by descending. Only one field can be specified.'
                       ' Do nothing if orderby was specified')\
    .add_argument("limit", type=inputs.positive, required=False, default=None, location='args',
                  help="numbers of games to return. Must be positive number.")\
    .add_argument('after', type=fields.date_from_iso8601, required=False, location='args',
                  help='date after which the game was played (ISO format YYYY-MM-dd)')\
    .add_argument('before', type=fields.date_from_iso8601, required=False, default=None, location='args',
                  help='date before which the game was played (ISO format YYYY-MM-dd)')

game_parser = RequestParser()\
    .add_argument('black', type=str, location='json', store_missing=False)\
    .add_argument('blackrank', type=str, location='json', store_missing=False)\
    .add_argument('white', type=str, location='json', store_missing=False)\
    .add_argument('whiterank', type=str, location='json', store_missing=False)\
    .add_argument('result', type=str, location='json', store_missing=False)\
    .add_argument('event', type=str, location='json', store_missing=False)\
    .add_argument('comment', type=str, location='json', store_missing=False)\
    .add_argument('date', type=fields.date_from_iso8601, location='json', store_missing=False) \
    .add_argument('name', required=False, type=str, location='json', store_missing=False) \
    .add_argument('place', type=str, location='json', store_missing=False)

game_model = api.model("Game", dict(
        black=fields.String(description="black player's name", max_length=64),
        blackrank=fields.String(description="black players's rank", example='3d', max_length=4),
        white=fields.String(description="white player's name", max_length=64),
        whiterank=fields.String(description="white players's rank", max_length=4),
        result=fields.String(description='result of the game', example='W+5.5'),
        event=fields.String,
        comment=fields.String,
        date=fields.DateTime(dt_format='iso8601', min=datetime(1, 1, 1), description="Date when the game was played"),
        name=fields.String(min_length=1, description='name of the game', max_length=128),
        place=fields.String('place where game was played', max_length=128),
        url=fields.Url('games_game_resource', attribute='id', readonly=True, description='url for the game information'),
        sgfurl=fields.Url('games_sgf_resource', attribute='id', readonly=True, description="url for game's sgf file")
))


def init(app, connection_string, connect_args={}):
    app.config['RESTPLUS_JSON'] = dict(ensure_ascii=False)
    api.init_app(app)
    # todo: репозиторий передавать параметром
    repository.init_connection(connection_string, connect_args)


def shutdown_session():
    repository.Session.remove()


@ns.route("/<int:id>/sgf")
class SGFResource(Resource):
    def get(self, id):
        """
        Return sgf file of the game record with specific id
        """
        name, sgf = repository.get_sgf_bytes(id)
        if sgf is None:
            raise NotFound("Game with id:{} was not found".format(id))

        response = Response(sgf, mimetype="application/octet-stream")

        response.headers["Content-Disposition"] = "attachment; filename*=UTF-8''{}.sgf".format(parse.quote(name))
        response.headers["charset"] = "utf8"
        return response


@ns.route("")
class GamesListResource(Resource):
    @ns.expect(upload_parser)
    @ns.marshal_with(game_model, code=201)
    @ns.response(code=400, description='There are no file in parameters')
    def post(self):
        """
        Add new game record from sgf file
        :return:
        """
        if FILE_PARAMETER_NAME not in request.files:
            raise BadRequest('There is no sgf file in the request')

        try:
            file = request.files[FILE_PARAMETER_NAME]
            return repository.save_game(file.stream, file.filename), 201
        except ValueError:
            raise BadRequest('File is not sgf file or corrupted')
        except Exception as ex:
            raise InternalServerError(str(ex))

    @ns.expect(search_parser)
    @ns.marshal_list_with(game_model, code=200)
    def get(self):
        query = repository.query_games()
        filter = search_parser.parse_args(strict=True)
        if filter.player:
            query = query.player(filter.player)

        if filter.event:
            query = query.event(filter.event)

        if filter.after or filter.before:
            query = query.between(filter.before, filter.after)

        if filter.order_by:
            query = query.order_by(filter.order_by)
        else:
            if filter.order_by_desc:
                query = query.order_by(filter.order_by_desc, is_desc=True)

        # todo: добавить skip
        if filter.limit:
            query = query.limit(filter.limit)

        records = query.all()
        return [record_to_dict(record) for record in records]


@ns.route("/<int:id>")
class GameResource(Resource):
    @ns.marshal_with(game_model, code=200)
    @ns.response(code=404, description='Game with specific id was not found.')
    def get(self, id):
        record = repository.get_game(id)
        if record is None:
            raise NotFound("Game with id:{} was not found".format(id))

        return record

    @ns.response(code=400, description='Validation error')
    @ns.response(code=500, description='Error during updating database')
    @ns.response(code=404, description='Game with specific id was not found.')
    @ns.response(code=200, description="Success")
    @ns.expect(game_model, validate=True)
    def put(self, id):
        if request.headers['Content-Type'] != 'application/json':
            return "Expected json data", 400

        try:
            if repository.update_game(id, game_parser.parse_args(strict=True)):
                return "OK", 200
            else:
                raise NotFound("Game with id:{} was not found".format(id))
        except repository.UpdateError as ex:
            raise InternalServerError(ex.message)
        except repository.ValidateError as vex:
            raise BadRequest(vex.message)

    @ns.response(code=404, description='Game with specific id was not found.')
    def delete(self, id):
        if repository.delete_game(id):
            return "OK", 200

        raise NotFound("Game with id:{} was not found".format(id))


def record_to_dict(record):
    return dict(
        black=record.black,
        blackrank=record.blackrank,
        white=record.white,
        whiterank=record.whiterank,
        result=record.result,
        event=record.event,
        comment=record.comment,
        date=record.date,
        name=record.name,
        place=record.place,
        id=record.id
    )
