import unittest
from io import BytesIO

from flask import json

import repository
from app import app

raw_test_data = [
    b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Comment_1]EV[Event_1]GN[Game_1]SZ[19]KM[7.5]PW[White]WR[9p]'
    b'PB[Black]BR[1p]DT[2017-01-01]PC[Place_1]RE[W+1])',

    b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Comment_2]EV[Event_2]GN[Game_2]SZ[19]KM[7.5]PW[White]WR[9p]'
    b'PB[Black]BR[1p]DT[2017-02-01]PC[Place_2]RE[W+2])',

    b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Comment_3]EV[Event_3]GN[Game_3]SZ[19]KM[7.5]PW[White]WR[9p]'
    b'PB[Black]BR[1p]DT[2017-03-01]PC[Place_3]RE[W+3])',

    b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Comment_4]EV[Event_4]GN[Game_4]SZ[19]KM[7.5]PW[White]WR[9p]'
    b'PB[Black]BR[1p]DT[2017-04-01]PC[Place_4]RE[W+4])',

    b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Comment_5]EV[Event_5]GN[Game_5]SZ[19]KM[7.5]PW[White]WR[9p]'
    b'PB[Black]BR[1p]DT[2017-05-01]PC[Place_5]RE[W+5])'
]


def setUpModule():
    repository.init_connection("sqlite://", {})
    repository.create_db()


class ApiV1AddTests(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()

    def tearDown(self):
        repository.Session.query(repository.GameRecord).delete()
        repository.Session.commit()
        repository.Session.remove()

    def test_add_game_ok(self):
        expected = dict(
            result='W+1',
            name='The 3rd SportAccord World Mind Games',
            white='SAWMG11',
            whiterank='9p',
            black='SAWMG11',
            blackrank='9p',
            place='IGS- PandaNet',
            comment='Russian womens',
            event='Womens',
            date='2013-12-14T00:00:00',
            url='/api/v1/games/1',
            sgfurl='/api/v1/games/1/sgf'
        )
        example = b'(;GM[1]FF[4]CA[UTF-8]ST[1]GC[Russian womens]EV[Womens]' \
                  b'GN[The 3rd SportAccord World Mind Games]SZ[19]KM[7.5]PW[SAWMG11]WR[9p]' \
                  b'PB[SAWMG11]BR[9p]DT[2013-12-14]PC[IGS- PandaNet]RE[W+1])'
        io = BytesIO(example)
        response = self.client.post("api/v1/games", data=dict(sgf=(io, 'file.sgf')))
        self.assertEqual(response.status_code, 201)
        actual = json.loads(response.data)
        self.assertEqual(expected, actual)

    def test_add_game_400_with_no_file(self):
        response = self.client.post("api/v1/games")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, b'{"message": "There is no sgf file in the request"}\n')

    def test_add_game_400_with_wrong_file(self):
        example = b'not sgf file'
        response = self.client.post("api/v1/games", data=dict(sgf=(BytesIO(example), 'file.sgf')))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, b'{"message": "File is not sgf file or corrupted"}\n')


class ApiV1GamesTests(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()
        for data in raw_test_data:
            io = BytesIO(data)
            response = self.client.post("api/v1/games", data=dict(sgf=(io, 'file.sgf')))
            self.assertEqual(201, response.status_code)

    def tearDown(self):
        repository.Session.query(repository.GameRecord).delete()
        repository.Session.commit()
        repository.Session.remove()

    def assertResponseEquals(self, response, code, json_data=None, data=None, message=None):
        self.assertEqual(response.status_code, code, msg=message or response.data)
        if json_data:
            actual = json.loads(response.data)
            self.assertEqual(json_data, actual, msg=message or response.data)

        if data:
            self.assertEqual(data, response.data, msg=message or response.data)

    def test_get_game_by_id(self):
        expected = dict(
            result='W+1',
            name='Game_1',
            white='White',
            whiterank='9p',
            black='Black',
            blackrank='1p',
            place='Place_1',
            comment='Comment_1',
            event='Event_1',
            date='2017-01-01T00:00:00',
            url='/api/v1/games/1',
            sgfurl='/api/v1/games/1/sgf'
        )
        response = self.client.get("api/v1/games/1")
        self.assertResponseEquals(response, code=200, json_data=expected)

    def test_get_game_wrong_id(self):
        expected = dict(message='Game with id:99 was not found')
        response = self.client.get("api/v1/games/99")
        self.assertResponseEquals(response, code=404, json_data=expected)

    def test_get_game_sgf(self):
        expected = raw_test_data[0]
        response = self.client.get("api/v1/games/1/sgf")
        self.assertResponseEquals(response, code=200, data=expected)

    def test_get_game_sgf_wrong_game(self):
        expected = dict(message='Game with id:99 was not found')
        response = self.client.get("api/v1/games/99/sgf")
        self.assertResponseEquals(response, code=404, json_data=expected)

    def test_delete_game(self):
        response = self.client.delete("api/v1/games/1")
        self.assertResponseEquals(response, code=200, message='Can''t delete game')

        response = self.client.get("api/v1/games/1")
        self.assertResponseEquals(response, code=404, message='Game is not deleted')

    def test_delete_wrong_game(self):
        expected = dict(message='Game with id:99 was not found')
        response = self.client.delete("api/v1/games/99")
        self.assertResponseEquals(response, code=404, json_data=expected)

    def test_edit_game_all_fields(self):
        data = dict(
            result='W+99',
            name='Game_edited',
            white='White_edited',
            whiterank='8p',
            black='Black_edited',
            blackrank='2p',
            place='Place_edited',
            comment='Comment_edited',
            event='Event_edited',
            date='2018-01-01T00:00:00'
        )
        response = self.client.put("api/v1/games/1", data=json.dumps(data), content_type='application/json')
        self.assertResponseEquals(response, code=200)

        expected = data.copy()
        expected['url'] = '/api/v1/games/1'
        expected['sgfurl'] = '/api/v1/games/1/sgf'
        response = self.client.get("api/v1/games/1")
        self.assertResponseEquals(response, code=200, json_data=expected)

    def test_edit_game_one_field(self):
        data = dict(result='W+99')
        response = self.client.put("api/v1/games/1", data=json.dumps(data), content_type='application/json')
        self.assertResponseEquals(response, code=200)

        expected = dict(
            result='W+99',
            name='Game_1',
            white='White',
            whiterank='9p',
            black='Black',
            blackrank='1p',
            place='Place_1',
            comment='Comment_1',
            event='Event_1',
            date='2017-01-01T00:00:00',
            url='/api/v1/games/1',
            sgfurl='/api/v1/games/1/sgf'
        )
        response = self.client.get("api/v1/games/1")
        self.assertResponseEquals(response, code=200, json_data=expected)

    def test_edit_game_wrong_game(self):
        data = dict(result='W+99')
        expected = dict(message='Game with id:99 was not found')
        response = self.client.put("api/v1/games/99", data=json.dumps(data), content_type='application/json')
        self.assertResponseEquals(response, code=404, json_data=expected)

    def test_query_games(self):
        expected = [
            dict(
                result='W+4',
                name='Game_4',
                white='White',
                whiterank='9p',
                black='Black',
                blackrank='1p',
                place='Place_4',
                comment='Comment_4',
                event='Event_4',
                date='2017-04-01T00:00:00',
                url='/api/v1/games/4',
                sgfurl='/api/v1/games/4/sgf'
            ),
            dict(
                result='W+3',
                name='Game_3',
                white='White',
                whiterank='9p',
                black='Black',
                blackrank='1p',
                place='Place_3',
                comment='Comment_3',
                event='Event_3',
                date='2017-03-01T00:00:00',
                url='/api/v1/games/3',
                sgfurl='/api/v1/games/3/sgf'
            )
        ]
        response = self.client.get(
            "api/v1/games?player=Black&before=2017-05-01&after=2017-01-31&limit=2&orderbydesc=date")
        self.assertResponseEquals(response, code=200, json_data=expected)

    def test_query_games_case_insensitive(self):
        expected = [
            dict(
                result='W+4',
                name='Game_4',
                white='White',
                whiterank='9p',
                black='Black',
                blackrank='1p',
                place='Place_4',
                comment='Comment_4',
                event='Event_4',
                date='2017-04-01T00:00:00',
                url='/api/v1/games/4',
                sgfurl='/api/v1/games/4/sgf'
            )
        ]
        response = self.client.get(
            "api/v1/games?event=event_4&player=black&limit=2&orderbydesc=date")
        self.assertResponseEquals(response, code=200, json_data=expected)

    def test_query_games_wrong_query_arg(self):
        expected = dict(message='Unknown arguments: unknown')
        response = self.client.get("api/v1/games?unknown=event_4&player=black")
        self.assertResponseEquals(response, code=400, json_data=expected)


if __name__ == '__main__':
    unittest.main()
