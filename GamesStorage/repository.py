from sqlalchemy import Column, Integer, String, DateTime, LargeBinary, Text
from sqlalchemy import create_engine
from sqlalchemy import desc
from sqlalchemy import or_
from sqlalchemy.exc import DataError, OperationalError
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import scoped_session, sessionmaker

# todo: подключить через свой репозиторий или через git
from sgftools import parser
from tools import try_date

# maybe: рассмотреть http://flask-sqlalchemy.pocoo.org/2.1/
Session = scoped_session(sessionmaker())
Base = declarative_base()
echo = True


def init_connection(connection_string, connection_args):
    engine = create_engine(connection_string, connect_args=connection_args, encoding='utf-8', echo=echo)
    Session.configure(bind=engine)


def create_db():
    # howto: найти как получить sql создания базы и разницы с уже существующей
    print(Base.metadata.create_all(Session().get_bind()))


class GameRecord(Base):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    version_id = Column(Integer, nullable=False)

    name = Column(String(length=128), nullable=False)
    comment = Column(Text)
    event = Column(String(64))
    black = Column(String(length=64), index=True)
    blackrank = Column(String(4))
    white = Column(String(length=64), index=True)
    whiterank = Column(String(4))

    result = Column(String(8), index=True)
    date = Column(DateTime, index=True)
    place = Column(String(128))

    sgf = Column(LargeBinary)

    __table_args__ = {"mysql_DEFAULT CHARSET": "UTF8"}

    __mapper_args__ = {"version_id_col": version_id}


class GamesQuery:
    def __init__(self, query):
        self.query = query

    def player(self, player_name):
        self.query = self.query.filter(or_(GameRecord.black.ilike(player_name), GameRecord.white.ilike(player_name)))
        return self

    def event(self, event_name):
        self.query = self.query.filter(GameRecord.event.ilike(event_name))
        return self

    def order_by(self, order_by, is_desc=False):
        column = desc(order_by) if is_desc else order_by
        self.query = self.query.order_by(column)
        return self

    def between(self, before_date, after_date):
        if before_date:
            self.query = self.query.filter(GameRecord.date <= before_date)

        if after_date:
            self.query = self.query.filter(GameRecord.date >= after_date)

        return self

    def limit(self, limit):
        self.query = self.query.limit(limit)
        return self

    def all(self):
        return self.query.all()


def generate_name(game_info, filename):
    if game_info.game_name:
        return game_info.game_name

    name = "{} - {}".format(game_info.white_player, game_info.black_player)
    if name != " - ":
        if game_info.event:
            name = "{} {}".format(name, game_info.event)

        if game_info.round:
            name = "{} {}".format(name, game_info.round)

        return name

    return filename


def save_game(stream, filename):
    # maybe: парсинг файла можно вынести в отдельный класс
    game = parser.SgfParser().load_game_from_stream(stream)
    game_info = game.game_info

    record = GameRecord()
    record.black = game_info.black_player
    record.blackrank = game_info.black_rank
    record.comment = game_info.game_comment
    record.event = game_info.event
    record.white = game_info.white_player
    record.whiterank = game_info.white_rank
    record.result = game_info.result
    record.place = game_info.place

    record.name = generate_name(game_info, filename)
    record.date = try_date(game_info.date, None, "%Y-%m-%d", '%d-%m-%Y', "%Y/%m/%d", '%d/%m/%Y', '%d.%m.%Y', '%Y.%m.%d')
    record.sgf = stream.getvalue()  # hack: всегда ли можно гарантировать наличие метода getvalue ?

    Session.add(record)
    Session.commit()
    tmpid = record.id  # hack: заставляем sqlalchemy получить id

    return record


class ValidateError(Exception):
     def __init__(self, message, *args, **kwargs):
        self.message = message
        super().__init__(args, kwargs)


class UpdateError(Exception):
    def __init__(self, message, *args, **kwargs):
        self.message = message
        super().__init__(args, kwargs)


game_updatable_fields = {'black', 'blackrank', 'white', 'whiterank', 'comment', 'name', 'result', 'date', 'place',
                         'event'}


# maybe https://colanderalchemy.readthedocs.io/en/latest/
# maybe https://attrs.readthedocs.io/en/stable/
def validate_game_fields(fields_dict):
    for x in fields_dict:
        if x not in game_updatable_fields:
            raise ValidateError("Field '{}' unknown".format(x))

    if ("name" in fields_dict) and (not fields_dict["name"]):
        raise ValidateError("Field 'name' can not be empty.")


def update_game(record_id, fields_dict):
    validate_game_fields(fields_dict)
    try:
        updated_count = Session.query(GameRecord).filter_by(id=record_id).update(fields_dict)
        Session.commit()
    except (DataError, OperationalError) as ex:
        raise UpdateError(str(ex.orig))

    return updated_count == 1


def query_games():
    # hack: добавление/удаление поля потребует здесь внимания
    return GamesQuery(Session.query(
        GameRecord.id, GameRecord.date, GameRecord.event, GameRecord.comment, GameRecord.name, GameRecord.place,
        GameRecord.result, GameRecord.white, GameRecord.whiterank, GameRecord.black, GameRecord.blackrank
    ))


def get_game(record_id):
    return Session.query(GameRecord).get(record_id)


def get_sgf_bytes(record_id):
    return Session.query(GameRecord.name, GameRecord.sgf).filter_by(id=record_id).first() or (None, None)


def delete_game(record_id):
    deleted_count = Session.query(GameRecord).filter_by(id=record_id).delete()
    Session.commit()
    return deleted_count > 0
