from flask import Flask
import api.v1 as v1

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['MAX_CONTENT_LENGTH'] = 5 * 1024
app.config['ERROR_404_HELP'] = False  # https://github.com/flask-restful/flask-restful/issues/449

app.config.from_pyfile('config.py', silent=True)


if 'CONNECTION_STRING' not in app.config:
    raise Exception('CONNECTION_STRING is not configured')

connection_args = app.config.get('CONNECTION_ARGS', {})

# todo: настроить лог

v1.init(app, app.config['CONNECTION_STRING'],  connection_args)


@app.teardown_appcontext
def shutdown_session(exception=None):
    v1.shutdown_session()


def main():
    app.run(host="0.0.0.0", debug=True, port=5001)


if __name__ == "__main__":
    main()
