const HtmlWebpackPlugin =require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const path =  require("path");
const webpack = require('webpack');

module.exports = {
    entry: path.resolve("./src/index.tsx"),
    output: {
        path: path.join(__dirname, "dist"),
        filename: "scripts/bundle.js"
    },

    resolve: {
        extensions: [".js", ".ts", ".tsx"]
    },

    module: {
        rules: [
            {
                test: /\.less$/,
                exclude: /node_modules/,
                loader: ExtractTextPlugin.extract([
                    { loader: "css-loader"},
                    { loader: "less-loader"}
                ])
            },

            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    { loader: "babel-loader" },
                    { loader: "ts-loader" }
                ]
            },

            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },

            {
                test: /\.(png|jpe?g|gif|svg)$/,
                exclude: /node_modules/,
                use: "file-loader?name=./images/[name].[ext]"
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin("./css/[name].[contenthash:8].css"),

        new HtmlWebpackPlugin({
            inject: true,
            template: "./public/index.html",
        }),

    ],

    target: "web"
};
