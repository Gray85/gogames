import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './css/index.less';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
