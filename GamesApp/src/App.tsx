import * as React from 'react';
import * as $ from 'jquery';
import * as injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import './css/App.less';

import GamesList from './GamesList';

const base_url = "http://localhost/sgfstorage";
const games_url = "http://localhost/sgfstorage/api/v1/games";

export default class App extends React.Component<any, any>
{
    constructor(props)
    {
        super(props);
        this.state = {error: false, games:  []};
        let setState = this.setState.bind(this);

        // https://learn.javascript.ru/xhr-crossdomain
        // https://enable-cors.org/server_nginx.html
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS


        $.ajax(games_url)
            .done(function(data)
            {
                console.log("success");
                setState({games: data});
            })
            .fail(function (jqXHR, textStatus, errorThrown)
            {
                console.log(textStatus);
                setState({error: true})
            });
    }

    render()
    {
        return (
          <div className="App">
            <div className="App-header">
              <h2>Go games repository</h2>
            </div>

              {
                  this.state.error ?
                      (<span>Не удалось получить данные сервера</span>):
                      (<GamesList baseUrl={base_url} games={this.state.games}/>)
              }

          </div>
        );
    }
}
