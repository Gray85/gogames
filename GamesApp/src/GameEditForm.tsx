import * as React from 'react';

import DatePicker from "material-ui/DatePicker"
import TextField from "material-ui/TextField"
import RaisedButton from 'material-ui/RaisedButton'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';


export default class GameEditForm extends React.Component<any, any>
{
    static childContextTypes = {
        muiTheme: React.PropTypes.object.isRequired,
    };

    onSubmit(event)
    {
        event.preventDefault()
    }

    onCancelClick(event)
    {
        if (this.props.onCancelClick)
            this.props.onCancelClick();

        event.preventDefault();
    }

    getChildContext()
    {
        return { muiTheme: getMuiTheme(baseTheme) };
    }

   render()
   {
       return (<form onSubmit={this.onSubmit.bind(this)}>
           <fieldset>
               <TextField fullWidth={true} name="name" hintText="Название игры" /> <br/>
               Игрок белыми: <TextField name="white"/> Уровень: <TextField name="whiterank" maxLength="3" type="" /> <br/>
               Игрок черными: <TextField name="black"/>  Уровень: <TextField name="blackrank" maxLength="3" type="" /> <br/>
               Дата игры: <DatePicker name="date"/>
               Место игры: <TextField name="place"/> Событие: <TextField name="event"/><br/>
               Результат: <TextField name="result" maxLength="4"/><br/>

               <TextField fullWidth={true} multiLine={true} hintText="Комментарий" name="comment" /><br/>

               <RaisedButton >Сохранить</RaisedButton>
               <RaisedButton onClick={this.onCancelClick.bind(this)}>Закрыть</RaisedButton>
           </fieldset>
       </form>)
   }
}


