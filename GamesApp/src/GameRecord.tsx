/**
 * Created by gray on 22.02.17.
 */

import * as React from 'react';
import GameEditForm from './GameEditForm'

export default class GameRecord extends React.Component<any, any>
{
    static edit_img  = require("./images/edit-info.png");
    static download_img = require("./images/download.png");
    constructor(props)
    {
        super(props);
        this.state = {edit: false}
    }

    onEditClick()
    {
        this.setState({edit: true})
    }

    onCancelClick()
    {
        this.setState({edit: false})
    }

    renderTableRow()
    {
        return (<tr>
            <td>{this.props.date}</td>
            <td>{this.props.white} {this.props.whiterank}</td>
            <td>{this.props.black} {this.props.blackrank}</td>
            <td>{this.props.result}</td>
            <td>{this.props.event}</td>
            <td><a href={ this.props.baseUrl + this.props.sgfurl} title="Загрузить файл sgf"><img alt="sgf"
                                                                                                  src={GameRecord.download_img}/></a>
            </td>
            <td><a href="#" onClick={this.onEditClick.bind(this)} title="Править информацию об игре"><img src={GameRecord.edit_img} alt="edit/view"/></a></td>
        </tr>);
    }

    render()
    {
        return this.state.edit ?
            (<tr><td colSpan={7}><GameEditForm onCancelClick={this.onCancelClick.bind(this)} /></td></tr>) :
            this.renderTableRow()
    }

}
