/**
 * Created by gray on 21.02.17.
 */

import * as React from 'react';
import GameRecord from './GameRecord';
import './css/game.less'

export default class GamesList extends React.Component<any, any>
{
    constructor(props)
    {
        super(props);
    }

    renderGameRecords()
    {
        return this.props.games.map((game, index) => <GameRecord baseUrl={this.props.baseUrl} {...game} />);
    }

    onSgfLoadClick(url)
    {
        if (this.props.onSgfLoadClick)
            this.props.onSgfLoadClick(url);

    }

    render()
    {

        return (<table className="GameRow">
            <thead>
            <tr>
                <th>Дата</th>
                <th >Белый</th>
                <th>Черный</th>
                <th>Результат</th>
                <th>Событие</th>
            </tr>
            </thead>
            <tbody>
                {this.renderGameRecords()}
            </tbody>

        </table>);
    }

}
